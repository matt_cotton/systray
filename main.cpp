#include "Systray.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Systray w;
	a.setQuitOnLastWindowClosed(false);
	return a.exec();
}
