#include "Systray.h"
#include <QtWidgets/QMenu>
#include <QtWidgets/QMessageBox>


Systray::Systray(QWidget *parent)
	: QDialog(parent)
	, exit_(false)
{
	ui.setupUi(this);

	// Create some actions
	QAction* open_action = new QAction("Open", this);
	connect(open_action, &QAction::triggered, this, &Systray::open_app);
	QAction* about_action = new QAction("About", this);
	connect(about_action, &QAction::triggered, this, &Systray::show_about);
	QAction* exit_action = new QAction("Exit", this);
	connect(exit_action, &QAction::triggered, this, &Systray::exit);

	// Create a menu and add the actions to it
	QMenu* tray_menu = new QMenu(this);
	tray_menu->addAction(open_action);
	tray_menu->addAction(about_action);
	tray_menu->addSeparator();
	tray_menu->addAction(exit_action);

	//Create the tray icon and add the menu to it
	tray_icon_ = new QSystemTrayIcon(this);
	tray_icon_->setContextMenu(tray_menu);
	tray_icon_->setToolTip("Example tool tip.");
	tray_icon_->setIcon(QIcon(":systray/blah.ico"));
	tray_icon_->show();

	// Used to capture tray menu double click and left click
	connect(tray_icon_, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
		this, SLOT(tray_activated(QSystemTrayIcon::ActivationReason)));

	// Tell people we are here
	tray_icon_->showMessage("Really cool tray icon", "Hello, here is a message from your really cool tray icon...");
}

Systray::~Systray()
{
}

void Systray::open_app()
{
	show();
	activateWindow();
	raise();
}

void Systray::show_about()
{
	QMessageBox::information(this, "About", "A brief message about this application...");
}

void Systray::exit()
{
	exit_ = true;
	close();
}

void Systray::closeEvent(QCloseEvent *event)
{
	if (!exit_)
	{
		hide();
		event->ignore();
	}
	else
	{
		// needed since we are setting setQuitOnLastWindowClosed(false)
		qApp->quit();
	}
}


void Systray::tray_activated(QSystemTrayIcon::ActivationReason reason)
{
	switch (reason)
	{
	case QSystemTrayIcon::DoubleClick:
		break;
	case QSystemTrayIcon::Trigger:
#ifdef WIN32
		// this causes two menus to pop up on osx
		tray_icon_->contextMenu()->popup(QCursor::pos());
		tray_icon_->contextMenu()->activateWindow();
#endif
		// fall through
	case QSystemTrayIcon::Context:
		break;
	default:
		break;
	}
}

