#ifndef SYSTRAY_H
#define SYSTRAY_H

#include <QtWidgets/QDialog>
#include <QtWidgets/QSystemTrayIcon>
#include <QCloseEvent>
#include "ui_systray.h"

class Systray : public QDialog
{
	Q_OBJECT

public:
	Systray(QWidget *parent = 0);
	~Systray();

public slots:
	void open_app();
	void show_about();
	void exit();
	void tray_activated(QSystemTrayIcon::ActivationReason reason);

protected: 
	void closeEvent(QCloseEvent *event);

private:
	Ui::SystrayClass ui;
	QSystemTrayIcon* tray_icon_;
	bool exit_;
};

#endif // SYSTRAY_H
